package testing

import (
	"bytes"
	"compress/gzip"
	"compress/zlib"
	"errors"
	"io"

	"github.com/gfx-labs/nbt"
)

type PaletteEntry struct {
	Name       string                 `nbt:"Name"`
	Properties map[string]interface{} `nbt:"Properties,omitempty"`
}

type Section struct {
	Y                        byte  `nbt:"Y"`
	StarlightBlocklightState int32 `nbt:"starlight.blocklight_state,omitempty"`
	StarlightSkylightState   int32 `nbt:"starlight.skylight_state,omitempty"`
	Biomes                   struct {
		Data    []int64  `nbt:"data"`
		Palette []string `nbt:"palette"`
	} `nbt:"biomes,omitempty"`
	BlockStates struct {
		Data    []int64        `nbt:"data"`
		Palette []PaletteEntry `nbt:"palette"`
	} `nbt:"block_states,omitempty"`
	BlockLight []byte `nbt:"BlockLight,omitempty"`
	SkyLight   []byte `nbt:"SkyLight,omitempty"`
}

type Column struct {
	DataVersion int32                    `nbt:"DataVersion"`
	XPos        int32                    `nbt:"xPos"`
	YPos        int32                    `nbt:"yPos"`
	ZPos        int32                    `nbt:"zPos"`
	Lights      [][]int16                `nbt:"Lights,omitempty"`
	Entities    []map[string]interface{} `nbt:"entities,omitempty"`
	Heightmaps  struct {
		MOTIONBLOCKING         []int64 `nbt:"MOTION_BLOCKING"`
		MOTIONBLOCKINGNOLEAVES []int64 `nbt:"MOTION_BLOCKING_NO_LEAVES"`
		OCEANFLOOR             []int64 `nbt:"OCEAN_FLOOR"`
		OCEANFLOORWG           []int64 `nbt:"OCEAN_FLOOR_WG"`
		WORLDSURFACE           []int64 `nbt:"WORLD_SURFACE"`
		WORLDSURFACEWG         []int64 `nbt:"WORLD_SURFACE_WG"`
	} `nbt:"Heightmaps"`
	InhabitedTime  int64           `nbt:"InhabitedTime"`
	LastUpdate     int64           `nbt:"LastUpdate"`
	PostProcessing [][]interface{} `nbt:"PostProcessing"`
	Status         string          `nbt:"Status"`
	BlockEntities  []interface{}   `nbt:"block_entities"`
	BlockTicks     []interface{}   `nbt:"block_ticks"`
	FluidTicks     []interface{}   `nbt:"fluid_ticks"`
	IsLightOn      byte            `nbt:"isLightOn"`
	Sections       []Section       `nbt:"sections"`
	CarvingMasks   struct {
		Air    []int64 `nbt:"AIR"`
		Liquid []int64 `nbt:"LIQUID"`
	} `nbt:"CarvingMasks"`
	StarlightLightVersion int32 `nbt:"starlight.light_version"`
	Structures            struct {
		References map[string]interface{} `nbt:"References"`
		Starts     map[string]interface{} `nbt:"starts"`
	} `nbt:"structures"`
}

// Load read column data from []byte
func (c *Column) Load(data []byte) (err error) {
	var r io.Reader = bytes.NewReader(data[1:])

	switch data[0] {
	default:
		err = errors.New("unknown compression")
	case 1:
		r, err = gzip.NewReader(r)
	case 2:
		r, err = zlib.NewReader(r)
	}

	if err != nil {
		return err
	}

	err = nbt.NewDecoder(r).Decode(c)
	return
}
